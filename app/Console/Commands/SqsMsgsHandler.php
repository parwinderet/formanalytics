<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

require 'vendor/autoload.php';
use Aws\Sqs\SqsClient;
use Aws\Exception\AwsException;
use App\Faform;

class SqsMsgsHandler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sqs:poll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Poll msgs from SQS queue and perform deletion on received msgs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

	    //

	    $queueUrl = "https://sqs.us-east-2.amazonaws.com/176250448218/cloudQueue";
	    $client = new SqsClient([
                            'profile' => 'default',
			    'region' => 'us-east-2',
			    'version' => 'latest'
	    ]);
	    try {
		    $result = $client->receiveMessage(array(
					    'AttributeNames' => ['SentTimestamp'],
					    'MaxNumberOfMessages' => 1,
					    'MessageAttributeNames' => ['All'],
					    'QueueUrl' => $queueUrl, // REQUIRED
					    'WaitTimeSeconds' => 20,
					    ));
		    if (count($result->get('Messages')) > 0) {
			    //var_dump($result->get('Messages')[0]);

                       /*Start:::Hooking code for DB insertion for the msgs received from SQS*/
                            //Retrieve Msgs from Fa_form table;
			    $forms = Faform::all();

			    foreach ($forms as $form) {
                                 echo "DB retrieval";
   				 echo $form->form_url;
			    }
                            //TBD: Parsing of received sqs msgs in to DB table;
                              
                            //Update FaForm table entry in DB;
                             $form = FaForm::updateOrCreate(
                                         ['customer_id' => 1],
					 ['form_url' => 'http://de111xx.com', 
                                          'num_of_fields' => 1, 'action' => 'delete', 'created_at' => '2018-08-30 13:15:41',
                                          'updated_at' => '2018-08-16 13:15:41', 'deleted_at' => '2018-08-15 13:15:41',
                                          'status' => 2]

				     );
                                  
                       /*End:::Hooking code for DB insertion for the msgs received from SQS*/


			    $result = $client->deleteMessage([
					    'QueueUrl' => $queueUrl, // REQUIRED
					    'ReceiptHandle' => $result->get('Messages')[0]['ReceiptHandle'] // REQUIRED
			    ]);


		    } else {
			    echo "No messages in queue. \n";
		    }
	    } catch (AwsException $e) {
		    // output error message if fails
		    error_log($e->getMessage());
	    }


    }
}
