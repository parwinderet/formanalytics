<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $form_id
 * @property boolean $is_lead
 * @property string $cookie_key
 * @property string $url
 * @property string $referrer
 * @property string $user_agent
 * @property string $visitor_ip
 * @property string $visited_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $status
 * @property FaForm $faForm
 * @property FaEvent[] $faEvents
 */
class Favisitor extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'fa_visitors';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['form_id', 'is_lead', 'cookie_key', 'url', 'referrer', 'user_agent', 'visitor_ip', 'visited_at', 'created_at', 'updated_at', 'deleted_at', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faForm()
    {
        return $this->belongsTo('App\FaForm', 'form_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function faEvents()
    {
        return $this->hasMany('App\FaEvent', 'visitor_id');
    }
}
