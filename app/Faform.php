<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property int $customer_id
 * @property string $form_url
 * @property boolean $num_of_fields
 * @property string $action
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $status
 * @property User $user
 * @property FaVisitor[] $faVisitors
 */
class Faform extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'fa_forms';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['customer_id', 'form_url', 'num_of_fields', 'action', 'created_at', 'updated_at', 'deleted_at', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function faVisitors()
    {
        return $this->hasMany('App\FaVisitor', 'form_id');
    }
}
