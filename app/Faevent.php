<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property integer $visitor_id
 * @property boolean $form_num
 * @property boolean $step_num
 * @property string $label
 * @property boolean $field_num
 * @property string $field_type
 * @property string $css_id
 * @property string $event_name
 * @property string $event_time
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $status
 * @property FaVisitor $faVisitor
 */
class Faevent extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'fa_events';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['visitor_id', 'form_num', 'step_num', 'label', 'field_num', 'field_type', 'css_id', 'event_name', 'event_time', 'created_at', 'updated_at', 'deleted_at', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faVisitor()
    {
        return $this->belongsTo('App\FaVisitor', 'visitor_id');
    }
}
