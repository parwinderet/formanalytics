<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fa_forms', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->integer('customer_id')->unsigned()->index('customer_id');
			$table->string('form_url', 200);
			$table->boolean('num_of_fields');
			$table->string('action', 100);
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fa_forms');
	}

}
