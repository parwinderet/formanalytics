<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaVisitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fa_visitors', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('form_id')->index('form_id');
			$table->boolean('is_lead');
			$table->string('cookie_key', 64);
			$table->string('url', 100);
			$table->string('referrer', 100);
			$table->string('user_agent', 100);
			$table->string('visitor_ip', 15)->nullable();
			$table->timestamp('visited_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fa_visitors');
	}

}
