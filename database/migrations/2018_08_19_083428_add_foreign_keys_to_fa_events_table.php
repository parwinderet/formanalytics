<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFaEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fa_events', function(Blueprint $table)
		{
			$table->foreign('visitor_id', 'fa_events_ibfk_1')->references('id')->on('fa_visitors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fa_events', function(Blueprint $table)
		{
			$table->dropForeign('fa_events_ibfk_1');
		});
	}

}
