<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFaVisitorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('fa_visitors', function(Blueprint $table)
		{
			$table->foreign('form_id', 'fa_visitors_ibfk_1')->references('id')->on('fa_forms')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('fa_visitors', function(Blueprint $table)
		{
			$table->dropForeign('fa_visitors_ibfk_1');
		});
	}

}
