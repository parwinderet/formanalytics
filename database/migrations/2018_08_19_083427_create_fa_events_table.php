<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFaEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fa_events', function(Blueprint $table)
		{
			$table->bigInteger('id', true);
			$table->bigInteger('visitor_id')->index('visitor_id');
			$table->boolean('form_num');
			$table->boolean('step_num');
			$table->string('label', 200)->nullable();
			$table->boolean('field_num');
			$table->string('field_type', 50);
			$table->string('css_id', 50)->nullable();
			$table->string('event_name', 50);
			$table->timestamp('event_time')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->timestamps();
			$table->softDeletes();
			$table->boolean('status')->default(1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fa_events');
	}

}
